const GeneSegitiga = () => {
  document.getElementById("result").innerHTML = "";
  let angka = document.getElementById("inputAngka").value;

  angka = angka.toString().split("");

  let result = "";

  for (let i = 0; i < Number.length(); i++) {
    result += angka[i];
  }

  document.getElementById("result").innerHTML = result;
};

const gBilangGanjil = () => {
  document.getElementById("result").innerHTML = "";
  let angka = document.getElementById("inputAngka").value;

  let ganjil = "";
  for (let i = 0; i <= angka; i++) {
    if (i % 2 == 0) {
      continue;
    } else {
      ganjil += i + " ";
    }
  }
  document.getElementById("result").innerHTML = ganjil;
};

const gBilanganPrima = () => {
  document.getElementById("result").innerHTML = "";
  let angka = document.getElementById("inputAngka").value;

  let bilPrima = "";
  for (let i = 1; i <= angka; i++) {
    if (cekBil(i)) {
      bilPrima += i + " ";
    }
  }
  document.getElementById("result").innerHTML = bilPrima;
};

const cekBil = (angka) => {
  if (angka == 1 || angka == 2) {
    return true;
  }
  for (let i = angka - 1; i > 1; i--) {
    if (angka % i == 0) {
      return false;
    } else {
      continue;
    }
  }
  return true;
};
